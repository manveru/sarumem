with import (
  fetchTarball {
    url = https://github.com/nixos/nixpkgs/archive/fe6bb17872a3b22ca9ce35b0fafd5acb3bab2e3a.tar.gz;
    sha256 = "01pavnx3g93nk2vvpmn7zg1kzic7rw4g7lqcxfm3z9nglv3xvs08";
  }
) {};
mkShell { buildInputs = [ mint cacert ]; }
