module Utils {
  fun randomArray (keep : Number) : Array(Maybe(Number)) {
    mkArray(40)
    |> shuffle()
    |> maskArray(keep)
  }

  fun maskArray (keep : Number, array : Array(Number)) : Array(Maybe(Number)) {
    Array.map(
      (num : Number) : Maybe(Number) {
        if (num > keep) {
          Maybe.nothing()
        } else {
          Maybe.just(num)
        }
      },
      array)
  }

  fun mkArray (length : Number) : Array(Number) {
    `
    (() => {
      return Array.from({ length: #{length} }).map((v, i) => i + 1)
    })()
    `
  }

  fun shuffle (array : Array(a)) : Array(a) {
    `
    (() => {
      var arr = #{array}.slice()
      var currentIndex = arr.length, temporaryValue, randomIndex

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = arr[currentIndex];
        arr[currentIndex] = arr[randomIndex];
        arr[randomIndex] = temporaryValue;
      }

      return arr
    })()
    `
  }

  fun getHighscore : Number {
    Storage.Local.get("highscore")
    |> Result.withDefault("0")
    |> Number.fromString()
    |> Maybe.withDefault(0)
  }

  fun setHighscore (n : Number) : Void {
    try {
      Storage.Local.set("highscore", Number.toString(n))
      void
    }
  }
}

/* Represents a subscription for `Provider.TinyTick` */
record Provider.TinyTick.Subscription {
  millis : Function(a)
}

/* A provider for periodic updated (every 1 seconds). */
provider Provider.TinyTick : Provider.TinyTick.Subscription {
  /* Updates the subscribers. */
  fun update : Array(a) {
    subscriptions
    |> Array.map(
      (item : Provider.TinyTick.Subscription) : Function(a) { item.millis })
    |> Array.map((func : Function(a)) : a { func() })
  }

  /* Attaches the provider. */
  fun attach : Void {
    `
    (() => {
      this.detach()
      this.id = setInterval(#{update}.bind(this), 100)
    })()
    `
  }

  /* Detaches the provider. */
  fun detach : Void {
    `clearInterval(this.id)`
  }
}

store Store {
  state level : Number = 3
  state numbers : Array(Maybe(Number)) = []
  state pressed : Array(Number) = []
  state lost : Bool = false
  state won : Bool = false
  state highscore : Number = Utils.getHighscore()
  state startTime : Number = 0
  state endTime : Number = 0

  fun start : Promise(Never, Void) {
    next
      {
        numbers = Utils.randomArray(nextLevel()),
        pressed = [],
        lost = false,
        won = false,
        level = nextLevel(),
        highscore = calculateHighscore(),
        startTime = 0
      }
  }

  fun calculateHighscore : Number {
    if (level > highscore) {
      level
    } else {
      highscore
    }
  }

  fun nextLevel : Number {
    if (won) {
      level + 1
    } else if (level > 3) {
      level - 1
    } else {
      level
    }
  }

  fun pressNumber (num : Number) : Function(Html.Event, Promise(Never, Void)) {
    (event : Html.Event) : Promise(Never, Void) {
      next { pressed = Array.push(num, pressed) }
    }
  }

  fun youLose (time : Number) : Promise(Never, Void) {
    next
      {
        lost = true,
        endTime = time
      }
  }

  fun youWin (time : Number) : Promise(Never, Void) {
    sequence {
      calculateHighscore()
      |> Utils.setHighscore()

      next
        {
          won = true,
          endTime = time
        }
    }
  }
}

component Fullscreen {
  property children : Array(Html) = []

  style fullscreen {
    position: absolute;
    display: grid;
    grid-template-rows: auto 1fr;
    grid-template-rows: 1fr;
    justify-items: center;
    align-items: center;
    font-size: 3em;
    width: 100vw;
    height: 100vh;
    background: #000;
    color: #fff;
  }

  fun render : Html {
    <div::fullscreen>
      <{ children }>
    </div>
  }
}

component Main {
  connect Store exposing {
    endTime,
    startTime,
    numbers,
    lost,
    won,
    level,
    highscore
  }

  style emoji {
    font-family: "EmojiSymbols";
    line-height: 1;
    font-weight: 100;
    font-size: 3em;
  }

  style start {
    font-size: 2em;
  }

  get highscoreAfterWinning : Number {
    Array.max([
      highscore,
      level
    ])
  }

  fun render : Html {
    if (lost) {
      <Fullscreen>
        <div::emoji>
          "🙊"
        </div>

        <{ summary }>
        <StartButton/>
      </Fullscreen>
    } else if (won) {
      <Fullscreen>
        <div::emoji>
          "🐵"
        </div>

        <{ summary }>
        <StartButton/>
      </Fullscreen>
    } else if (Array.isEmpty(numbers)) {
      <Fullscreen>
        <div::start>
          "Start a new game"
        </div>

        <{ "Highscore: " + Number.toString(highscore) }>
        <StartButton/>
      </Fullscreen>
    } else {
      <Fullscreen>
        <Game/>
      </Fullscreen>
    }
  }

  get summary : Html {
    <dl::dl>
      <dt::dt>
        "Highscore"
      </dt>

      <dd::dd>
        <{ Number.toString(highscoreAfterWinning) }>
      </dd>

      <dt::dt>
        "Time"
      </dt>

      <dd::dd>
        <{ Number.toString((endTime - startTime) / 10) }>
        "s"
      </dd>
    </dl>
  }

  style dl {
    font-size: 0.5em;
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 1fr 1fr;
    width: 25vw;
  }

  style dd {

  }

  style dt {
    &:after {
      content: ":";
    }
  }
}

component StartButton {
  connect Store exposing { start }

  style start {
    max-width: 50vw;
    max-height: 50vh;
  }

  fun render : Html {
    <svg::start
      viewBox="0 0 100 100"
      onClick={start}>

      <circle
        fill="#00000000"
        stroke="#ffffffbb"
        stroke-width="1"
        cx="50"
        cy="50"
        r="25"/>

      <polygon
        fill="#ffffffbb"
        points="40,70 70,50 40,30"/>

    </svg>
  }
}

component Game {
  connect Store exposing { numbers, pressed, level, pressNumber, youLose, youWin }

  state time : Number = 0

  use Provider.TinyTick {
    millis =
      () : Promise(Never, Void) {
        next { time = time + 1 }
      }
  }

  style cells {
    display: grid;
    grid-template-rows: repeat(5, 1fr);
    grid-template-columns: repeat(8, 1fr);
    justify-items: center;
    align-items: center;
    justify-self: stretch;
    align-self: stretch;
  }

  style empty {
    display: grid;
    align-content: center;
    justify-content: center;
    justify-self: center;
    align-self: center;

    height: 17vh;
    width: 9vw;
    cursor: pointer;
  }

  style board {
    width: 9vw;
    height: 17vh;
  }

  get checkerboard : Html {
    <svg::board
      viewBox="0 0 8 8"
      preserveAspectRatio="none">

      <path
        d="M0,0V8H1V0zM2,0V8H3V0zM4,0V8H5V0zM6,0V8H7V0zM8,0V8H8V0zM0,0H8V1H0zM0,2H8V3H0zM0,4H8V5H0zM0,6H8V7H0zM0,8H8V8H0zM0"
        fill="#fff"
        fill-rule="evenodd"/>

    </svg>
  }

  fun render : Html {
    if (Array.isEmpty(pressed)) {
      <div::cells>
        for (num of numbers) {
          if (Maybe.isNothing(num)) {
            <div::empty/>
          } else {
            showNumber(num, true)
          }
        }
      </div>
    } else {
      <div::cells>
        for (num of numbers) {
          if (Maybe.isNothing(num)) {
            <div::empty onClick={(event : Html.Event) : Promise(Never, Void) { youLose(time) }}>
              <{ checkerboard }>
            </div>
          } else {
            showNumber(num, false)
          }
        }
      </div>
    }
  }

  fun showNumber (m : Maybe(Number), revealed : Bool) : Html {
    if (revealed) {
      case (num) {
        previous + 1 =>
          <div::empty onClick={pressNumber(num)}>
            <{ Number.toString(num) }>
          </div>

        =>
          <div::empty onClick={(event : Html.Event) : Promise(Never, Void) { youLose(time) }}>
            <{ Number.toString(num) }>
          </div>
      }
    } else if (previous + 1 == level && num == previous + 1) {
      <div::empty onClick={(event : Html.Event) : Promise(Never, Void) { youWin(time) }}>
        <{ checkerboard }>
      </div>
    } else if (num == previous + 1) {
      <div::empty onClick={pressNumber(num)}>
        <{ checkerboard }>
      </div>
    } else if ((num - 1) < previous) {
      <div::empty/>
    } else {
      <div::empty onClick={(event : Html.Event) : Promise(Never, Void) { youLose(time) }}>
        <{ checkerboard }>
      </div>
    }
  } where {
    num =
      Maybe.withDefault(-1, m)

    previous =
      Array.lastWithDefault(0, pressed)
  }
}
