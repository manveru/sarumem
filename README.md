# SaruMem

A little game I made after stumbling upon [a memory game for chimpanzees](https://www.cell.com/current-biology/fulltext/S0960-9822(07)02088-X).

The goal is to memorize the location of numbers on a field of 40 tiles. While I
don't have access to the original study or the
[follow-up study](https://link.springer.com/article/10.1007%2Fs10071-008-0206-8)
that showed that humans may attain the same level of accuracy and speed as a
chimpanzee, it's a fun little game for short breaks anyway.

You can try it at https://sarumem.manveru.dev/

This is written in the [Mint Language](https://www.mint-lang.com/).

You can build this using `mint build`.

Development is easiest using `mint start --auto-format`.

Pull requests welcome :)

## Acknowledgements

EmojiSymbols Font (c)blockworks - Kenichi Kaneko
http://emojisymbols.com/
